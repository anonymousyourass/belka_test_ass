﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Pool/PoolObject")]
public class PoolObject : MonoBehaviour {

    #region Interface
    public void ReturnToPool () 
    {
        gameObject.SetActive (false);
        if (gameObject.GetComponent<CollisionStatus>() != null)
        {
            gameObject.GetComponent<CollisionStatus>().inCollision = false;
            gameObject.GetComponent<CollisionStatus>().lastTriggerName = string.Empty;
            gameObject.GetComponent<CollisionStatus>().lastCollisionName = string.Empty;
        }
    }
    #endregion
}