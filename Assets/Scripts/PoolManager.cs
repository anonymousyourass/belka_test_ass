﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class PoolManager{
    private static PoolPart[] pools;
    private static GameObject objectsParent;
    
    [System.Serializable]
    public struct PoolPart
    {
        public string name; //имя префаба
        public PoolObject prefab; //сам префаб, как образец
        public int count; //количество объектов при инициализации пула
        public ObjectPooling ferula; //сам пул
    }
    
    public static void Initialize(PoolPart[] newPools) {
        pools = newPools; //заполняем информацию
        objectsParent = new GameObject ();
        objectsParent.name = "Pool"; //создаем на сцене объект Pool, чтобы не заслонять иерархию
        for (int i=0; i<pools.Length; i++) {
            if(pools[i].prefab!=null) {  
                pools[i].ferula = new ObjectPooling(); //создаем свой пул для каждого префаба
                pools[i].ferula.Initialize(pools[i].count, pools[i].prefab, objectsParent.transform); 
//инициализируем пул заданным количество объектов
            }
        }
    }

    public static List<PoolObject> ObjectsFromPoolActive(string name)
    {
        if (pools != null) {
            for (int i = 0; i < pools.Length; i++) {
                if (string.Compare (pools [i].name, name) == 0) { //если имя совпало с именем префаба пула
                    return pools[i].ferula.Objects().Where(o=>o.gameObject.activeInHierarchy).ToList();
                }
            }
        } 
        
        Debug.LogError("There is no pool named" + name);
        
        return null; //если такого нет пула, вернет null
    }
    
    public static GameObject GetObject (string name, Vector3 position, Quaternion rotation) {
        GameObject result = null;
        if (pools != null) {
            for (int i = 0; i < pools.Length; i++) {
                if (string.Compare (pools [i].name, name) == 0) { //если имя совпало с именем префаба пула
                    result = pools[i].ferula.GetObject ().gameObject; //дергаем объект из пула
                    result.transform.position = position;
                    result.transform.rotation = rotation; 
                    result.SetActive (true); //выставляем координаты и активируем
                    return result;
                }
            }
        } 
        return result; //если такого объекта нет в пулах, вернет null
    }
}