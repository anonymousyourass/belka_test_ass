﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public PlayerInput input;
    
    public GameObject overScreen;
    public Button overButton;
    public Text over_score;
    public Text over_distance;

    public GameObject startScreen;
    public Button startButton;

    public Image hit;
    public GameObject GamePlayScreen;
    public Text score;
    public Text distance;

    private const int  NUM_PLATF_SCREEN = 20;
    private const int  HEIGHT_CORRIDOR = 5;
    public static float SPEED = 14f;
    
    private const string  BEST_DISTANCE_PP = "distance";
    private const string  BEST_SCORE_PP = "score";


    private Dictionary<string,GameObject> lastGenerated = new Dictionary<string, GameObject>();
    

    private float spikeWindow = SPEED * 2f;
    private float generateObstacleCounter = 0f;
    
    private float gameScore = 0;
    private float gameDistance = 0;


    private Vector3 gravityUp = new Vector3(0f, 10, 0f);
    private Vector3 gravityDown = new Vector3(0f, -10, 0f);
    
    void Start()
    {
        startScreen.SetActive(true);
        Physics.gravity = gravityDown;
    }

    public void OnStartPress()
    {
        startScreen.SetActive(false);
        GamePlayScreen.SetActive(true);

        var player = PoolManager.GetObject("player", Vector3.zero, Quaternion.identity);
        
        if(lastGenerated.ContainsKey("player") == false)
            lastGenerated.Add("player",player);
        else
            lastGenerated["player"] = player;

        input.onPlayerInput += PlayerInput;
    }

    public void OnOverPress()
    {
        overScreen.SetActive(false);
        startScreen.SetActive(true);

        gameDistance = 0;
        gameScore = 0;
    }

    public void OnGameOver()
    {
        lastGenerated = new Dictionary<string, GameObject>();
        var returnToPoolTags = new[] {"player", "platform", "spike", "over"};
        ReturnToPool(returnToPoolTags,poolObject => true);
        GamePlayScreen.SetActive(false);
        overScreen.SetActive(true);

        var highScore = PlayerPrefs.GetFloat(BEST_SCORE_PP, 0) > gameScore ? PlayerPrefs.GetFloat(BEST_SCORE_PP, 0) : gameScore;
        var bestDistance = PlayerPrefs.GetFloat(BEST_DISTANCE_PP, 0) > gameScore ? PlayerPrefs.GetFloat(BEST_DISTANCE_PP, 0) : gameDistance;

        PlayerPrefs.SetFloat(BEST_SCORE_PP,highScore);
        PlayerPrefs.SetFloat(BEST_DISTANCE_PP,bestDistance);
        
        over_score.text = "BEST SCORE " + highScore.ToString();
        over_distance.text = "BEST DISTANCE "+ bestDistance.ToString("F1");
    }

    private void PlayerInput()
    {
        if (lastGenerated.ContainsKey("player") && lastGenerated["player"].GetComponent<CollisionStatus>().inCollision)
        {
            if (lastGenerated["player"].transform.position.y < 0)
            {
                Physics.gravity = gravityUp;
            }
            else
            {
                Physics.gravity = gravityDown;
            }
        }
    }
    
    void Update()
    {
        if (startScreen.activeSelf == false && GamePlayScreen.activeSelf == true)
        {
            gameDistance += Time.deltaTime * SPEED;
            
            //считаем сколько необходимо догенерировать платформ
            var startXOffset = 0;
            var intCount = 0;
        
            if (lastGenerated.ContainsKey("platform") == false)
            {
                intCount = (int) NUM_PLATF_SCREEN;
            }
            else if (lastGenerated["platform"].transform.position.x < NUM_PLATF_SCREEN)
            {
                var count = NUM_PLATF_SCREEN - lastGenerated["platform"].transform.position.x;
                intCount = (int) count;
                startXOffset = NUM_PLATF_SCREEN - intCount;
            }

            //гененируем недостающие платформы по количеству заполняющие экран
            var obj = new[] {"platform", string.Empty};
            
            var tagPosRotToGen = Enumerable.Range(0, intCount).Select(j =>
            {
                var randomWithWeight = UnityEngine.Random.Range(0, obj.Length);
                if (UnityEngine.Random.Range(0, 1f) > (0.1f))
                    randomWithWeight = 0;
                var poolTag =  obj[randomWithWeight];
                
                var pos = Vector3.up * (UnityEngine.Random.Range(0f,1f) > 0.5f ? HEIGHT_CORRIDOR : -HEIGHT_CORRIDOR) + (startXOffset + j) * Vector3.right;
                var rot = Quaternion.identity;
                return new Tuple<string, Vector3, Quaternion>(poolTag, pos, rot);
            });
            
            tagPosRotToGen.Generate(tagPosRot => string.IsNullOrEmpty(tagPosRot.Item1) == false).Include(lastGenerated);

            //убираем лишние платформы в пул
            var returnToPoolTags = new[] {"platform", "spike", "over"}.Where(s => lastGenerated.ContainsKey(s));
            ReturnToPool(returnToPoolTags.ToArray(),poolObject => poolObject.transform.position.x < -NUM_PLATF_SCREEN);

            if (lastGenerated.ContainsKey("player"))
            {
                generateObstacleCounter -= Time.deltaTime*SPEED;
                if (generateObstacleCounter <= lastGenerated["player"].transform.position.x)
                {
                    generateObstacleCounter = UnityEngine.Random.Range(spikeWindow, spikeWindow * 2f);
                
                    //считаем сколько необходимо догенерировать шипов = расстояние прыжка
                    var t = Mathf.Sqrt(HEIGHT_CORRIDOR * 2f / Mathf.Abs(Physics.gravity.y));
                    var jumpGenerateCountMax = (int) (SPEED * t * 0.9f);
                    var jumpCount = UnityEngine.Random.Range(1, (int) (jumpGenerateCountMax + 1));

                    var obs = new[] {"spike", "over"};
                    
                    var obsTagPosRotToGen = Enumerable.Range(0, jumpCount).Select(j =>
                    {
                        var randomWithWeight = UnityEngine.Random.Range(0, obs.Length);
                        if (UnityEngine.Random.Range(0, 1f) > (0.2f))
                            randomWithWeight = 0;
                        var pooltag = obs[randomWithWeight];
                        
                        var rnd = UnityEngine.Random.Range(0f, 1f) > 0.5f;
                        var pos = Vector3.up *  (rnd ? HEIGHT_CORRIDOR - 1 : -HEIGHT_CORRIDOR + 1) + ((int) (lastGenerated["player"].transform.position.x + generateObstacleCounter) + j) * Vector3.right;
                        var rot = rnd ? Quaternion.Euler(90f,0f,0f) : Quaternion.Euler(-90f,0f,0f);
                        return new Tuple<string,Vector3, Quaternion>(pooltag,pos, rot);
                    });
                    
                    obsTagPosRotToGen.Generate(objTagPos => string.IsNullOrEmpty(objTagPos.Item1) == false).Include(lastGenerated);
                }

                //столкновения с шипами
                if (lastGenerated["player"].GetComponent<CollisionStatus>().lastTriggerName.Contains("risk"))
                {
                    gameScore++;
                }
                //столкновения с платформой выход за пределы экрана
                if (lastGenerated["player"].GetComponent<CollisionStatus>().lastTriggerName.Contains("over") ||
                    lastGenerated["player"].transform.position.y > HEIGHT_CORRIDOR 
                    || lastGenerated["player"].transform.position.y < -HEIGHT_CORRIDOR)
                {
                    OnGameOver();
                }
            }
            
            score.text = "SCORE " + gameScore.ToString();
            distance.text = "DISTANCE " + gameDistance.ToString("F1");
            
        }
    }
    
    void ReturnToPool(string[] objTags,Func<PoolObject,bool> filterPoolObject)
    {
        foreach (var o in objTags.
            Select(PoolManager.ObjectsFromPoolActive).
            SelectMany(poolObjList=>poolObjList.ToArray()).Where(filterPoolObject))
        {
            o.ReturnToPool();
        }
    }

    public void HitOn()
    {
        hit.enabled = true;
    }
    
    public void HitOff()
    {
        hit.enabled = false;
    }
}
