﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions 
{
    public static void Include(this IEnumerable<Tuple<string,Vector3,Quaternion,GameObject>> generated,Dictionary<string,GameObject> generatedLast)
    {
        foreach (var gen in generated)
        {
            if (generatedLast.ContainsKey(gen.Item1) == false)
                generatedLast.Add(gen.Item1,gen.Item4);
            else
                generatedLast[gen.Item1] = gen.Item4;
        }
        
    }
    
}
