﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionStatus : MonoBehaviour
{
    public bool inCollision;
    public string lastCollisionName;
    public string lastTriggerName;
    
    void OnCollisionStay(Collision collisionInfo)
    {
        inCollision = true;
        lastCollisionName = collisionInfo.contacts[0].otherCollider.transform.name;
    }
    
    void OnCollisionEnter(Collision collisionInfo) {
        inCollision = true;
        lastCollisionName = collisionInfo.contacts[0].otherCollider.transform.name;
    }
    
    void OnCollisionExit(Collision collisionInfo) {
        inCollision = false;
        lastCollisionName = string.Empty;
    }
    
    
    void OnTriggerEnter(Collider other) {
        lastTriggerName = other.transform.name;
    }
    
    void OnTriggerExit(Collider other) {
        lastTriggerName = string.Empty;
    }
}
