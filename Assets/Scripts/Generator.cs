﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Generator 
{
    public static IEnumerable<Tuple<string,Vector3,Quaternion,GameObject>> Generate(this IEnumerable<Tuple<string,Vector3,Quaternion>> objs,Func<Tuple<string,Vector3,Quaternion>,bool> filter)
    {
        foreach (var s in objs.Where(filter))
        {
            yield return new Tuple<string, Vector3, Quaternion, GameObject>(s.Item1,s.Item2,s.Item3,PoolManager.GetObject(s.Item1, s.Item2, s.Item3));
        }
    }
}
