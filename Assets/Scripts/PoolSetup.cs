﻿using UnityEngine;
using System.Collections;



[AddComponentMenu("Pool/PoolSetup")]
public class PoolSetup : MonoBehaviour {//обертка для управления статическим классом PoolManager
	
    
    #region Unity scene settings
#pragma warning disable 0649
    [SerializeField] private PoolManager.PoolPart[] pools; //структуры, где пользователь задает префаб для использования в пуле и инициализируемое количество
#pragma warning restore 0649
    #endregion

    #region Methods
    void OnValidate() {
        for (int i = 0; i < pools.Length; i++) {
            pools[i].name = pools[i].prefab.name; //присваиваем имена заранее, до инициализации
        }
    }

    void Awake() {
        Initialize ();
    }

    void Initialize () {
        PoolManager.Initialize(pools); //инициализируем менеджер пулов
    }
    #endregion

}